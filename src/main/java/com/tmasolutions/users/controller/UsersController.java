package com.tmasolutions.users.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.endpoint.web.annotation.RestControllerEndpoint;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmasolutions.users.model.entity.AppUser;
import com.tmasolutions.users.model.response.ResponseModel;
import com.tmasolutions.users.service.Impl.AppUserService;

@RestController
@RequestMapping("/users")
public class UsersController {
	
	@Autowired
	AppUserService userServiceImpl;
	
	@Value("${service.instance.name}")
	private String serviceName;
	
	@Value("${server.port}")
	private String servicePort;
	
	@GetMapping("/instance-name")
	public ResponseModel createUser() {		
		 return new ResponseModel(true, "Service name: " + this.serviceName + ", Port: " + this.servicePort);    
    }   
	
 	@PostMapping("")
	public ResponseEntity<AppUser> createUser(@RequestBody AppUser user) throws JsonProcessingException {
    	AppUser dt = userServiceImpl.createNewUser(user);
        return ResponseEntity.ok(dt);
    
    }    
	    
    @GetMapping("/{id}")
    public ResponseModel one(@PathVariable Long id) {    
    	try {
    		AppUser usr = userServiceImpl.findById(id);
        	return new ResponseModel(true, usr);
    	}catch(Exception e) {
        	return new ResponseModel(false, null, e.getMessage());
    	}
    	
    }
}
