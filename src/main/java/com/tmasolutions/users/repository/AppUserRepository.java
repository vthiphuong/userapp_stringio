package com.tmasolutions.users.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.tmasolutions.users.model.entity.AppUser;


public interface AppUserRepository extends JpaRepository<AppUser, Long>  {
	List<AppUser> findByEmail(String Email);
	Page<AppUser> findAll(Pageable pageable);
	Page<AppUser> findByEmailContaining(String Email, Pageable pageable);
}
