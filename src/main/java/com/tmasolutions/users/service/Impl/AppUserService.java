package com.tmasolutions.users.service.Impl;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.tmasolutions.users.model.entity.AppUser;
import com.tmasolutions.users.repository.AppUserRepository;
import com.tmasolutions.users.service.IAppUserService;


@Service
public class AppUserService implements IAppUserService {

	@Autowired
	AppUserRepository userRepository;

	@Override
	public AppUser createNewUser(AppUser usr) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		usr.setPassword(passwordEncoder.encode(usr.getPassword()));
		// TODO Auto-generated method stub
		return userRepository.save(usr);
	}

	@Override
	public List<AppUser> loadUserByUsername(String Username) {
		// TODO Auto-generated method stub
		return userRepository.findByEmail(Username);
	}

	@Override
	public Page<AppUser> findAll(Pageable pageable) {
		return userRepository.findAll(pageable);
	}

	@Override	
	public Page<AppUser> findByEmailContaining(String email, Pageable pageable) {
		return userRepository.findByEmailContaining(email, pageable);
	}

	@Cacheable(value = "AppUser", key = "#id")
	@Override
	public AppUser findById(Long id) {
		AppUser usr = userRepository.findById(id).orElseThrow(() -> new RuntimeException("User Not Found"));
		return usr;
	}

	@Override
	public AppUser updateUser(Long id, AppUser newAppUser) {
		AppUser bk = userRepository.findById(id).orElseThrow(() -> new RuntimeException("User Not Found"));
		bk.setFirstname(newAppUser.getFirstname());
		bk.setLastname(newAppUser.getLastname());
		bk.setRole(newAppUser.getRole());
		userRepository.save(bk);
		return bk;
	}

	@Override
	public void deleteAppUser(Long id) {
		AppUser usr = userRepository.findById(id).orElseThrow(() -> new RuntimeException("User Not Found"));
		userRepository.delete(usr);
	}

}
