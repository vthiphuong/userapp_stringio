#!/bin/bash
export project_name="erp-backend"
export image_name="registry.gitlab.com/meusolutions/erp-backend"
export port_mapping="5000"
export mount_data_folder="/mnt/data"
export environment_name="staging"
export environment_json_path="/home/gitlab-runner/ERP_Backend_config/.env.staging.json"
